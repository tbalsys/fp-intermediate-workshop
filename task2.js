/*
 * TASK #2
 * Implement a function that will take groups array and return number of members in all active groups.
 * Assume that about 100 members in each group are duplicated.
 */

const groups = [
    { name: 'JavaScript', isActive: true, members: 700 },
    { name: 'Angular', isActive: false, members: 500 },
    { name: 'Node', isActive: true, members: 400 },
    { name: 'React', isActive: true, members: 600 },
];

const members = groups
    .filter(i => i.isActive)
    .map(i => i.members - 100)
    .reduce((sum, i) => sum + i, 0);

console.log(members);