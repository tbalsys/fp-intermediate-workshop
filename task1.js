/*
 * TASK #1
 * Refactor following code in a functional programming way
 */

const arr = [1, 2, 3, 4, 5, 6, 7, 8, 9];

const getOdds = (nums) => nums.filter(i => i % 2 !== 0)

console.log(getOdds(arr));