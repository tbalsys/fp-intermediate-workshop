/*
 * TASK #
 * Implement a snakeCase function in a point-less style using function composition.
 * Apply it for a list of strings.
 */

const strings = [
    'I love functional programming',
    ' This is probably THE BEST workshop EVER ',
];

const compose = (...fns) => x => fns.reduceRight((y, f) => f(y), x);

const toLower = (word) => word.toLowerCase();

const snakeCase = (str) => str
    .trim()
    .split(' ')
    .map(toLower)
    .join('_');

const strTrim = str => str.trim();
const spaceSplit = str => str.split(' ');
const arrToLower = arr => arr.map(toLower);
const underscoreJoin = arr => arr.join('_');

const snakeCasePointFree = compose(underscoreJoin, arrToLower, spaceSplit, strTrim);

console.log('non point free', strings.map(snakeCase));
console.log('point free', strings.map(snakeCasePointFree));
